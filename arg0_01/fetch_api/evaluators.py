from . import models 
from . import utils 


class ARGEvaluator:

    def __init__(self):
        ...

    def evaluate_get(self):
        ...

    def evaluate_post(self):
        ...

class GraphEvaluator(ARGEvaluator):

    def __init__(self):
        super().__init__()

    def evaluate_get(self):

        data = {} 
        data["nodes_count"] = utils.count_nodes()[0]
        data["rels_count"] = utils.count_relationships()[0]
        return data

        
    def evaluate_post(self, data):

        self.result_dict = data[0]
        self.nodes = data[1]
        self.node_pairs = data[2]

        self.rels = [] 
        for node in self.nodes:
            node.save()

        # saving relationships
        
        count = 0
        for rel_dict in self.result_dict["rels"]:
            first_node, second_node = self.node_pairs[count]

            first_node_id = first_node.get_node_id()
            second_node_id = second_node.get_node_id()
            rel_id = rel_dict["rel_id"]
            rel_type = rel_dict["a_type"] 
            rel_constraint = rel_dict["constraints"]
            first_node.add_rel(first_node_id, second_node_id, rel_type, rel_id, rel_constraint)
            count += 1
            
            """
            rel = first_node.associations.connect(second_node)
            rel.a_type = rel_dict["a_type"]
            rel.rel_id = rel_dict["rel_id"]
            self.rels.append(rel)
            count += 1
            """
        print("done")
        """
        for rel in self.rels:
            rel.save()
        self.rels = []
        """


class PatternsEvaluator(ARGEvaluator):

    def __init__(self):
        super().__init__()
        
    def evaluate_post(self):
        super().evaluate_post()


class QueryEvaluator(ARGEvaluator):

    def __init__(self):
        super().__init__()
        
    def evaluate_post(self, graph_data, query_data):
        
        # TODO: 
        self.result_dict = graph_data[0]
        self.nodes = graph_data[1]
        self.node_pairs = graph_data[2]
        self.query_param = query_data[0] 
        self.query_type = query_data[1]
