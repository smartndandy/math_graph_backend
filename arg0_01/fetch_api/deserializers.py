import json 
from . import utils 
from . import models 

class ARGDeserializer:

    def __init__(self, result):
        
        self.result = {}
        if isinstance(result.data, str):
            self.result = json.loads(result.data)
    
    def post_is_valid(self): 
        ...

    def post_save(self):
        ...


class GraphDeserializer(ARGDeserializer):

    def __init__(self, result):
        super().__init__(result) 

    def post_is_valid(self):
        super().post_is_valid()

        self.nodes = []
        self.nodes_set = set()
        self.node_pairs = [] 
        result_dict = self.result
        
        try:
            sentence_ids = []
            for node_dict in result_dict["nodes"]:
                node_id = node_dict["node_id"]
                node_term = node_dict["term"]
                node_type = node_dict["node_type"]

                node = None
                

                if node_type == "meta_doc":
                    node = models.DocNode(node_id=node_id, term = node_term)
                elif node_type == "meta_sentence":
                    node = models.SentenceNode(node_id=node_id, term = node_term)
                    if node_id in sentence_ids:
                        print("duplicant: ", node_term)
                    else:
                        sentence_ids.append(node_id)
                elif node_type == "meta_chap":
                    node = models.ChapterNode(node_id=node_id, term = node_term)
                elif node_type == "meta_section":
                    node = models.SectionNode(node_id=node_id, term = node_term)
                elif node_type == "instance":
                    node = models.TermInstanceNode(node_id=node_id, term = node_term)
                elif node_type == "entity":
                    node = models.Entity(node_id=node_id, term = node_term) 
                elif node_type == "sent_intent" or node_type == "sect_intent":
                    node = models.Intention(node_id=node_id, term = node_term)
                else:
                    print("node_type unknown: ", node_type)

                #node = models.Entity(node_id=node_id, term = node_term)
                self.nodes.append(node)
                #self.nodes_set.add(node)
            
            # creating node pairs...       
            count = 0
            for rel_dict in result_dict["rels"]:
                first_node_id = rel_dict["node1"]
                second_node_id = rel_dict["node2"]
                
                """
                if count > 48:
                    print("some")
                """
                
                first_node = [node_x for node_x in self.nodes if node_x.node_id == first_node_id][0] 

                second_node = [node_x for node_x in self.nodes if node_x.node_id == second_node_id][0]  
                
                """
                for node_x in self.nodes:
                    if node_x.get_node_id() == first_node_id:
                        first_node = node_x
                for node_x in self.nodes:
                    if node_x.get_node_id() == second_node_id:
                        second_node = node_x
                """
                
                self.node_pairs.append((first_node, second_node))
                count += 1
            

        except:
            self.nodes = []
            self.node_pairs = [] 
            return False    

        return True

    def get_post_data(self):

        return (self.result, self.nodes, self.node_pairs) 




class PatternsDeserializer(ARGDeserializer):

    def __init__(self, result):
        super().__init__(result) 

    def post_is_valid(self):
        super().post_is_valid()

    def post_save(self):
        super().post_save() 

class QueryDeserializer(ARGDeserializer):

    def __init__(self, result):
        super().__init__(result) 

    def post_is_valid(self):
        super().post_is_valid()

        self.nodes = []
        self.node_pairs = [] 
        self.query_param = None 
        self.query_type = None
        result_dict = self.result
        
        try:
            self.query_param = result_dict["query_param"] 
            self.query_type = result_dict["query_type"]

            for node_dict in result_dict["nodes"]:
                node_id = node_dict["node_id"]
                node_term = node_dict["term"]
                node = models.ARG_node(node_id=node_id, term = node_term)
                self.nodes.append(node)
            
            for rel_dict in result_dict["rels"]:
                first_node_id = rel_dict["node1"]
                second_node_id = rel_dict["node2"]
                
                first_node = [node_x for node_x in self.nodes if node_x.node_id == first_node_id][0] 
                second_node = [node_x for node_x in self.nodes if node_x.node_id == second_node_id][0]             
                self.node_pairs.append((first_node, second_node))

        except:
            self.nodes = []
            self.node_pairs = [] 
            self.query_param = None 
            self.query_type = None
            return False    

        return True

    def get_post_data(self):

        return (self.result, self.nodes, self.node_pairs), (self.query_param, self.query_type) 

    def post_save(self):
        super().post_save() 

    