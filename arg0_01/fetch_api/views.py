from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response 
from rest_framework import status 

from . import models
#from . import deserializer
from . import deserializers
from . import evaluators

# Create your views here.

def sample_data(request):
    models.create_sample_data()
    

#alternatively the response can be constructed explicitly:
"""
data = {
    'response': {
        'status': status.HTTP_200_OK,
        'data': count,
        },
}
"""

class GraphAPIView(APIView):

    def get(self, request):
        
        try:
            g_evaluator = evaluators.GraphEvaluator() 
            data = g_evaluator.evaluate_get()
            return Response(data, status=status.HTTP_200_OK)
        except:
            return Response("ERROR", status.HTTP_400_BAD_REQUEST)


    def post(self, request, format=None):  
        d_ser = deserializers.GraphDeserializer(request)

        if d_ser.post_is_valid():
            g_evaluator = evaluators.GraphEvaluator() 
            deserialized_data = d_ser.get_post_data()
            g_evaluator.evaluate_post(deserialized_data)

            return Response("received", status=status.HTTP_201_CREATED)

        
        return Response("ERROR", status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        ...


class PatternsAPIView(APIView):
    ...


class QueryAPIView(APIView):

    def post(self, request, format=None):

        d_ser = deserializers.QueryDeserializer(request)

        if d_ser.post_is_valid():
            # g_evaluator = evaluators.GraphEvaluator() 
            # deserialized_data = d_ser.get_post_data()
            # g_evaluator.evaluate_post(deserialized_data)
            # #d_ser.post_save()
            #d_ser.post_save() 
            q_evaluator = evaluators.QueryEvaluator() 
            graph_data, query_data = d_ser.get_post_data()
            data = q_evaluator.evaluate_post(graph_data, query_data)
            

            return Response(data, status=status.HTTP_202_ACCEPTED)

        return Response("ERROR", status=status.HTTP_400_BAD_REQUEST)
        

       
