#from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'fetch_api'

"""
urlpatterns = [
    path('', views.sample_data, name='sample_data'),
    path('nodes_count/', views.GetNodesCount.as_view(), name='get_nodes_count'),
    path('nodes/', views.PostNodesWithRels.as_view(), name='post_node_with_rels')
]
"""
urlpatterns = [
    path('', views.sample_data, name='sample_data'),
    path('graph/', views.GraphAPIView.as_view(), name='graph_api_view'),
    path('patterns/', views.PatternsAPIView.as_view(), name='patterns_api_view'),
    path('query/', views.QueryAPIView.as_view(), name='query_api_view')
]


#path('', views.sample_data, name='sample_data')


#path('nodes_count/', views.GetNodesCount.as_view(), name='get_nodes_count')#,
#path('nodes/', views.PostNodesWithRels.as_view(), name='post_node_with_rels')