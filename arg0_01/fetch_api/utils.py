from neomodel import db   
from . import models
import json


def eval_graph_get(request):
    result_data = {}
    #result = json.loads(request.data)
    if "count_nodes" in request:
        result_data["count_nodes"] = count_nodes() 
    if "count_relationships" in request: 
        result_data["count_rels"] = count_relationships() 
    return result_data


def count_relationships():
    rels_count = db.cypher_query(
        """
        MATCH ()-[r]->()
        RETURN count(r) as count
        """
    )
    rels_count_val = rels_count[0][0]
    return rels_count_val

def count_nodes():
    node_count = db.cypher_query(
        """
        MATCH (n)
        RETURN count(n) as count
        """
    )
    node_count_val = node_count[0][0]
    return node_count_val


def save_nodes(nodes):
    ...
    
def save_rels(rels):
    ...