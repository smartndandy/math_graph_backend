from django.db import models
from neomodel import StructuredNode, StringProperty, Relationship, StructuredRel

# Create your models here.

# ARG_node class definition. Every node captures a term 

# node types - all derived from arg_node
# arg_node 
# -> meta: -> doc,                   type: meta-doc
#          -> sentence,              type: meta-sentence
#          -> chapter,               type: meta-chapter
#          -> section                type: meta-section
# -> doc:  -> example node           type: example_instance
# -> sem_node: -> entity (operator)  type: entity
#              -> intention          type: sent_intent
#                                    type: sect_intent
# -> temp_node:

# rel types - all derived from AssociationRel 
# association_rel
# -> flow_to, sentence_flow_to, section_flow_to, document_flow_to (text_flow)
# -> ex_rel (examples of instances)
# -> associations (semantic rels)

# class definitions for meta nodes:
class META(StructuredNode):
    
    node_id = StringProperty(unique_index = True)
    term = StringProperty()

    def get_node_id(self):
        return str(self.node_id)

    def add_rel(self, node1_id, node2_id, rel_type, rel_id, rel_constraint):

        temp = {}
        temp["node1_id"] = node1_id 
        temp["node2_id"] = node2_id 
        temp["rel_type"] = rel_type 
        temp["rel_id"] = rel_id
        temp["rel_constraint"] = rel_constraint
        
        result = self.cypher("MATCH (n {node_id: $node1_id})," + "(m {node_id: $node2_id})" + "CREATE (n)-[:META_REL {a_type: $rel_type, rel_id: $rel_id, rel_constraint: $rel_constraint}]->(m)", temp)


class DocNode(META):
    ...

class SentenceNode(META):
    ...

class ChapterNode(META):
    ...

class SectionNode(META):
    ...

# class definitions for instance nodes:
class INSTANCE(StructuredNode):
    
    node_id = StringProperty(unique_index = True)
    term = StringProperty()

    def get_node_id(self):
        return str(self.node_id)

    def add_rel(self, node1_id, node2_id, rel_type, rel_id, rel_constraint):

        temp = {}
        temp["node1_id"] = node1_id 
        temp["node2_id"] = node2_id 
        temp["rel_type"] = rel_type 
        temp["rel_id"] = rel_id
        temp["rel_constraint"] = rel_constraint

        if rel_type == "flow_to" or rel_type == "sentence_flow_to":
            result = self.cypher("MATCH (n {node_id: $node1_id})," + "(m {node_id: $node2_id})" + "CREATE (n)-[:FLOW_TO {a_type: $rel_type, rel_id: $rel_id, rel_constraint: $rel_constraint}]->(m)", temp)
        elif rel_type == "ex_of":
            result = self.cypher("MATCH (n {node_id: $node1_id})," + "(m {node_id: $node2_id})" + "CREATE (n)-[:META_EX_INSTANCE_REL {a_type: $rel_type, rel_id: $rel_id, rel_constraint: $rel_constraint}]->(m)", temp)
        else:
            result = self.cypher("MATCH (n {node_id: $node1_id})," + "(m {node_id: $node2_id})" + "CREATE (n)-[:REST {a_type: $rel_type, rel_id: $rel_id, rel_constraint: $rel_constraint}]->(m)", temp)


class TermInstanceNode(INSTANCE):
    ...

# class definitions for semantic nodes:
class SEMANTIC(StructuredNode):
    
    node_id = StringProperty(unique_index = True)
    term = StringProperty()

    def get_node_id(self):
        return str(self.node_id)

    def add_rel(self, node1_id, node2_id, rel_type, rel_id, rel_constraint):

        temp = {}
        temp["node1_id"] = node1_id 
        temp["node2_id"] = node2_id 
        temp["rel_type"] = rel_type 
        temp["rel_id"] = rel_id
        temp["rel_constraint"] = rel_constraint
        
        result = self.cypher("MATCH (n {node_id: $node1_id})," + "(m {node_id: $node2_id})" + "CREATE (n)-[:SEM_ASSOCIATION {a_type: $rel_type, rel_id: $rel_id, rel_constraint: $rel_constraint}]->(m)", temp)


    

class Entity(SEMANTIC):
    ...


class Operator(Entity):
    ...

class Intention(SEMANTIC):
    ...




# class definitions for temporary nodes:
class TEMP(StructuredNode):
    
    node_id = StringProperty(unique_index = True)
    term = StringProperty()


"""
class AssociationRel(StructuredRel):

    a_type = StringProperty()
    rel_id = StringProperty()

    def set_a_type(self, _a_type):
        a_type = _a_type

class ARG_node(StructuredNode):

    node_id = StringProperty(unique_index = True)
    term = StringProperty()

    associations = Relationship('ARG_node', "ASSOCIATION", model=AssociationRel)
"""

"""
# A method for creating sample data
def create_sample_data():

    MATH = ARG_node(node_id = "n0", term = "Mathematik").save()
    ALG = ARG_node(node_id = "n1", term = "Algebra").save()
    GEOM = ARG_node(node_id = "n2", term = "Geometrie").save()

    rel1 = MATH.associations.connect(ALG)
    rel1.set_a_type("is_atom_of")
    rel1.rel_id = "r0"
    rel1.save()

    rel2 = MATH.associations.connect(GEOM)
    rel2.set_a_type("is_atom_of")
    rel2.rel_id = "r1"
    rel2.save()
"""